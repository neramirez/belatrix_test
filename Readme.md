# Find it

## Dependencies

* Java 11 `sudo apt-get install openjdk-11-jdk openjdk-11-jre`
* Maven 3.5 >  

## Build

```shell script
mvn clean install
```
 
## Running the project

| Parameter         | Description                                               | Sample                                        |
|-------------------|-----------------------------------------------------------|-----------------------------------------------|
| file              | The input filed to read urls                              | /tmp/testFile.txt                             |
| outputDir         | The output dir in which results will be stored            | /tmp/                                         |
| patterns          | A list of comma separated patterns  we want to search for | Trump,Golf                                    |

After build a local version of this artifact should be installed on your local maven repository, to run it you can

```shell script
java -jar  ~/.m2/repository/com/neramirez/belatrix/found-it/1.0.0-SNAPSHOT/found-it-1.0.0-SNAPSHOT.jar --file=/tmp/testFile.txt --outputDir=/tmp --patterns=Donald Trump

```

