package com.neramirez.belatrix;

import com.neramirez.belatrix.service.FindItComponent;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
@Log4j2
public class FindItApplication {
    /**
     *
     */


    /**
     * @param service The Autowired FindItComponent
     * @return
     */
    @Bean
    public CommandLineRunner run(@Autowired FindItComponent service) {

        log.info("*** ----------------------------- ****** ----------------------------- ***");
        return args -> {
            service.search();
        };


    }

    public static void main(String[] args) {
        SpringApplication.run(FindItApplication.class, args);
    }
}
