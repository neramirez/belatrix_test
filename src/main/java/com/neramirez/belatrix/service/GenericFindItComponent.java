package com.neramirez.belatrix.service;

import com.neramirez.belatrix.processor.ProcessorTemplate;
import com.neramirez.belatrix.processor.WebSiteProcessor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Set;

@Component
@Log4j2
public class GenericFindItComponent implements FindItComponent {

    private final String fileLocation;
    private final Set<String> patterns;
    private final String outputDir;


    /**
     * @param fileLocation The file location to be read
     * @param patterns     A comma separated list of patters to search for
     * @param outputDir    An output directory to store pattern occurrences
     */
    public GenericFindItComponent  (
            @Value("${file}") String fileLocation,
            @Value("${patterns}") String patterns,
            @Value("${outputDir}") String outputDir

    ) {
        this.fileLocation = fileLocation;
        this.outputDir = outputDir;
        this.patterns = Set.of(patterns.split(","));

        if (!StringUtils.hasText(fileLocation)) {
            throw new UnsupportedOperationException("Please provide a file parameter with `--file=/path/to/file`");

        } else {

            final File urlsFile = FileUtils.getFile(fileLocation);

            if (!urlsFile.exists()) {
                log.error("Specified input file doesnt exists, exiting");
                throw new UnsupportedOperationException("Specified input file doesnt exists, exiting");
            }

            final File outputDirFile = FileUtils.getFile(outputDir);


            if (!outputDirFile.exists()) {
                log.error("Specified output directory doesnt exists, exiting");
                throw new UnsupportedOperationException("Specified output directory doesnt exists, exiting");
            }


        }
    }

    @Override
    public void search() {
        try {
            FileUtils
                    .readLines(new File(fileLocation), Charset.defaultCharset())
                    .stream()
                    .map(url -> {
                        ProcessorTemplate processor = WebSiteProcessor.of(url, patterns, outputDir);
                        return processor;
                    })
                    .parallel()
                    .forEach(ProcessorTemplate::process);


        } catch (IOException e) {
            log.error("An IO Error has occurred while reading input file", e);
        }
    }
}
