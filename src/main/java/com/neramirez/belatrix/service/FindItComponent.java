package com.neramirez.belatrix.service;


/**
 * Interfaces that defines FindIt service behavior
 */
public interface FindItComponent {

    void search();
}
