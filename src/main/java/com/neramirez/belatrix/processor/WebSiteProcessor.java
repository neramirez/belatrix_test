package com.neramirez.belatrix.processor;

import com.neramirez.belatrix.model.Occurrence;
import lombok.Value;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@Log4j2
public class WebSiteProcessor extends ProcessorTemplate {

    private WebSiteProcessor(
            final String url,
            final Set<String> patterns,
            final String outputDir) {
        super(url, patterns, outputDir);

    }

    public static WebSiteProcessor of(
            final String url,
            final Set<String> patterns,
            final String outputDir) {
        return new WebSiteProcessor(url, patterns, outputDir);
    }

    @Override
    String getDocument() throws IOException {
        log.info("Obtaining document at:" + this.getUrl());

        return Jsoup.connect(this.url).get().toString();
    }

    /**
     * @param document
     * @return
     */
    @Override
    Map<String, List<Occurrence>> search(final String document) {

        final String[] documentLines = document.split("\n");

        return patterns
                .stream()
                .collect(
                        Collectors
                                .toMap(
                                        pattern -> pattern,
                                        pattern -> searchPatternInDocument(pattern, documentLines)
                                )
                );
    }


    /**
     * @param pattern       Pattern to search for in the document
     * @param documentLines Input to search in
     * @return A list of occurrences found of pattern in the document.
     * An occurrence is defined as the line and column number the occurrence was found
     * If no occurrences are found a single String is returned `pattern + " was not found inside document at:" + url`
     */
    private List<Occurrence> searchPatternInDocument(final String pattern, final String[] documentLines) {
        log.info("Looking for `" + pattern + "` on " + this.getUrl());

        List<Occurrence> occurrences = new LinkedList<>();
        for (int i = 0; i < documentLines.length; i++) {
            String line = documentLines[i];

            int index = line.indexOf(pattern);
            while (index != -1) {

                occurrences.add(Occurrence.builder().row(i + 1).column(index).build());
                line = line.substring(index + pattern.length());
                index = line.indexOf(pattern);
            }
        }
        return occurrences;
    }


}
