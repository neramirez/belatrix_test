package com.neramirez.belatrix.processor;


import com.neramirez.belatrix.model.Occurrence;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
@Log4j2
public abstract class ProcessorTemplate {
    protected final String url;
    protected final Set<String> patterns;
    protected final String outputDir;

    /**
     * @return
     * @throws IOException on error
     */
    abstract String getDocument() throws IOException;

    /**
     * @param document The document gathered from the GET request to the URL
     * @return A Map that contains a list of occurrences for each patterns
     */
    abstract Map<String, List<Occurrence>> search(final String document);

    private List<File> write(final Map<String, List<Occurrence>> results) {
        return results
                .keySet()
                .stream()
                .map(pattern -> {

                    List<Occurrence> occurrences = results.get(pattern);


                    String filename = (this.url + "_" + pattern).replaceAll("[^a-zA-Z0-9\\.\\-]", "_");
                    File output = new File(this.outputDir, filename);
                    log.info("Storing results  of pattern on:" + output);

                    try {
                        FileUtils.writeLines(output, mapOccurrencesToStrings(occurrences));
                    } catch (IOException e) {
                        log.error("An error occurred while writing to file system", e);
                    }
                    return output;
                }).collect(Collectors.toList());
    }

    /**
     *
     */
    public void process() {

        try {
            final String document = getDocument();
            final Map<String, List<Occurrence>> results = search(document);
            write(results);
        } catch (IllegalArgumentException | IOException e) {
            log.error("An error has occurred while connecting to " + url + "... Skipping... ", e);
        }
    }


    private List<String> mapOccurrencesToStrings(List<Occurrence> occurrences) {
        return
                occurrences
                        .stream()
                        .map(occurrence -> "Found at Line:" + (occurrence.getRow()) + " Column:" + occurrence.getColumn())
                        .collect(Collectors.toList());
    }
}
