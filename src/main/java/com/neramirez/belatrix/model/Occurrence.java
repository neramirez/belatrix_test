package com.neramirez.belatrix.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Occurrence {
    final Integer column;
    final Integer row;
}
