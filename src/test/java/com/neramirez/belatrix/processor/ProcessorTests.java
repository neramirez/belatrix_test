package com.neramirez.belatrix.processor;

import com.neramirez.belatrix.model.Occurrence;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RunWith(SpringRunner.class)
public class ProcessorTests {
    @Test
    public void whenGetDocument_thenReturnStringBody() {
        // given
        ProcessorTemplate template = WebSiteProcessor.of("https://www.google.com", Set.of(""), null);
        //when
        String googleDocument = null;
        try {
            googleDocument = template.getDocument();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //assert
        Assert.notNull(googleDocument, "Document should not be null");
    }

    @Test(expected = UnknownHostException.class)
    public void whenGetDocumentWithNonExistingDomain_thenThrowIOException() throws IOException {
        // given
        ProcessorTemplate templateUnknownHost = WebSiteProcessor.of("http://unknwonhost.com.co.zz", Set.of(""), null);
        //when
        templateUnknownHost.getDocument();


    }


    @Test(expected = IllegalArgumentException.class)
    public void whenGetDocumentWithBadURL_thenThrowIllegalArgumentException() {
        // given
        ProcessorTemplate templateBadUrl = WebSiteProcessor.of("bad_url", Set.of(""), null);
        //when
        try {
            templateBadUrl.getDocument();
            //assert

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void whenGetDocumentWithBadUrl_thenReturnNullBody() {
        // given
        ProcessorTemplate templateBadUrl = WebSiteProcessor.of("bad_url", Set.of(""), null);
        ProcessorTemplate templateUnknownHost = WebSiteProcessor.of("http://unknwonhost.com.co.zz", Set.of(""), null);
        //when
        String nullBadUrl = null;
        try {
            nullBadUrl = templateBadUrl.getDocument();
            String nullUnknownHost = templateUnknownHost.getDocument();
            //assert
            Assert.isNull(nullBadUrl, "Document should  be null");
            Assert.isNull(nullUnknownHost, "Document should  be null");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void whenSearchWithResults_thenReturnMap() {
        // given
        ProcessorTemplate template = WebSiteProcessor.of("https://www.google.com", Set.of("ing"), null);
        //when
        Map<String, List<Occurrence>> searchResults = template.search("testing\ningredient");
        //assert
        Assert.isTrue(searchResults.size() == 1, "Search should return 1 list per pattern");
        Assert.isTrue(searchResults.get("ing").size() == 2, "Search should return 2 items");
    }


    @Test
    public void whenSearchWithoutResults_thenReturnMap() {
        String pattern = "NON EXISTING PATTERN";
        String url = "https://www.google.com";
        // given
        ProcessorTemplate template = WebSiteProcessor.of(url, Set.of(pattern), null);
        //when
        Map<String, List<Occurrence>> searchResults = template.search("testing\ningredient");
        //assert
        Assert.isTrue(searchResults.size() == 1, "Search should return 1 key per pattern");
        Assert.isTrue(searchResults.get(pattern).size() == 0, "Search should return no items");
        Assert.isTrue(searchResults.get(pattern).get(0).equals(pattern + " was not found inside document at:" + url));

    }


}
